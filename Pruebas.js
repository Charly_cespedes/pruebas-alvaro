const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Creación de Conexión

const Pool = require('pg').Pool

/* const pool = new Pool({

  user: 'postgres',

  host: '10.10.10.23',

  database: 'RSLogistic',

  password: 'SAPB1Admin',

  port: 5432,

})
 */
/// Creacion de Arreglos 

let Login = {
 User:'',
 Password: '',
 IMEI: '',
 Dato: ''
};

let respuesta = {
 error: false,
 codigo: 200,
 mensaje: ''
};


app.get('/', function(req, res) {
 respuesta = {
  error: true,
  codigo: 200,
  mensaje: 'Punto de inicio'
 };
 res.send(respuesta);
});


app.use(function(req, res, next) {
 respuesta = {
  error: true, 
  codigo: 404, 
  mensaje: 'URL no encontrada'
 };
 res.status(404).send(respuesta);
});
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});