const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Creación de Conexión

const Pool = require('pg').Pool

const pool = new Pool({

  user: 'postgres',

  host: '10.10.10.23',

  database: 'RSLogistic',

  password: 'SAPB1Admin',

  port: 5432,

})

/// Creacion de Arreglos 

let Login = {
 User:'',
 Password: '',
 IMEI: '',
 Dato: ''
};

let respuesta = {
 error: false,
 codigo: 200,
 mensaje: ''
};


app.get('/', function(req, res) {
 respuesta = {
  error: true,
  codigo: 200,
  mensaje: 'Punto de inicio'
 };
 res.send(respuesta);
});


app.get('/usuario', function (req, res) {
 respuesta = {
  error: false,
  codigo: 200,
  mensaje: ''
 };
 if(Login.nombre === '' || Login.apellido === '') {
  respuesta = {
   error: true,
   codigo: 501,
   mensaje: 'El usuario no ha sido creado'
  };
 } else {
  respuesta = {
   error: false,
   codigo: 200,
   mensaje: 'respuesta del usuario',
   respuesta: usuario
  };
 }
 res.send(respuesta);
});



app.post('/usuario', function (req, res) {
 if(!req.body.User || !req.body.Password) {
  respuesta = {
   error: false,
   codigo: 502,
   mensaje: 'El campo usuario y contraseña son requeridos'
  };
 } else 
 {
    const User_lo = req.body.User
    const Pass = req.body.Password
    const IMEI = req.body.IMEI

    pool.query('SELECT * FROM "Users" WHERE Username = ($1)', [User_lo],  (error, results) => {
        if (error) {
          throw error
        } else
        {
            respuesta = {
                error: true,
                codigo: 200,
                mensaje: 'Usuario extrae '
               };
        }
       // response.status(200).json(results.rows)
     
        
      })
 }
 
 res.send(respuesta);
});



app.put('/usuario', function (req, res) {
 if(!req.body.nombre || !req.body.apellido) {
  respuesta = {
   error: true,
   codigo: 502,
   mensaje: 'El campo nombre y apellido son requeridos'
  };
 } else {
  if(Login.nombre === '' || Login.apellido === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'El usuario no ha sido creado'
   };
  } else {
   Login = {
    nombre: req.body.nombre,
    apellido: req.body.apellido
   };
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Usuario actualizado',
    respuesta: usuario
   };
  }
 }
 
 res.send(respuesta);
});
app.delete('/usuario', function (req, res) {
 if(Login.nombre === '' || Login.apellido === '') {
  respuesta = {
   error: true,
   codigo: 501,
   mensaje: 'El usuario no ha sido creado'
  };
 } else {
  respuesta = {
   error: false,
   codigo: 200,
   mensaje: 'Usuario eliminado'
  };
  Login = { 
   nombre: '', 
   apellido: '' 
  };
 }
 res.send(respuesta);
});
app.use(function(req, res, next) {
 respuesta = {
  error: true, 
  codigo: 404, 
  mensaje: 'URL no encontrada'
 };
 res.status(404).send(respuesta);
});
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});